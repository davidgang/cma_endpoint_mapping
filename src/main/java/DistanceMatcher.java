import com.google.common.collect.ImmutableList;

import java.util.Collections;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Stream;

import static java.lang.Math.abs;

public class DistanceMatcher {
    MatcherUtils utils = new MatcherUtils();
    ImmutableList<Endpoint> sortedList;

    DistanceMatcher() {
        Stream<Endpoint> endpoints = utils.parseFile("/home/david/freightos/git/ce-main/b/CNDoors.csv");
        sortedList = endpoints.sorted(Comparator.comparingDouble(Endpoint::getLongitude)).collect(ImmutableList.toImmutableList());
    }

    Optional<Endpoint> match(double distance, double lat, double lon) {
        int whole_lon = (int) lon;
        double lon_dist_in_m = utils.distance(lat,lat, whole_lon,whole_lon-1,0,0);
        double lon_dist_in_km = lon_dist_in_m/1000;
        double lon_dist_in_rad = distance/lon_dist_in_km;
        double lat_dist_in_rad = distance/110;
        Endpoint dummy = new Endpoint("","","",lat,lon - lon_dist_in_rad);
        int index = Collections.binarySearch(sortedList,dummy,Comparator.comparing(Endpoint::getLongitude));
        int first_i = abs(index);
        while (first_i>0 && (sortedList.get(first_i).getLongitude() - sortedList.get(first_i-1).getLongitude())<0.001){
            first_i--;
        }
        ImmutableList.Builder<Endpoint> builder = ImmutableList.builder();
        for(int i=first_i; i<sortedList.size(); i++){
            Endpoint e= sortedList.get(i);
            if(e.getLongitude() - lon > lon_dist_in_rad) {
                break;
            }
            if(abs(e.getLatitude() - lat) <= lat_dist_in_rad) {
                builder.add(e);
            }
        }
    return utils.getNearestPoint(builder.build(), lat,lon);

    }


}
