import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;

public class CityMatcher {
    Map<String,List<Endpoint>> cityMultiMap ;
    MatcherUtils utils = new MatcherUtils();
    CityMatcher(){
        Stream<Endpoint> endpoints = utils.parseFile("/home/david/freightos/git/ce-main/b/CNDoors.csv");
        cityMultiMap = endpoints.collect(groupingBy(Endpoint::getCity));
    }
    Optional<Endpoint> match(String city, double lat, double lon) {
        List<Endpoint> endpoints = cityMultiMap.get(city);
        if(endpoints == null) {
            return Optional.empty();
        }
        return utils.getNearestPoint(endpoints,lat,lon);
    }
}
