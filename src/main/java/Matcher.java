import java.util.Optional;

public class Matcher {

    public static void main(String[] args) {
        CityMatcher matcher1 = new CityMatcher();
        long start = System.currentTimeMillis();
        Optional<Endpoint> point = matcher1.match("NINGBO", 29.8831156, 121.574495);
        System.out.println(System.currentTimeMillis() - start);
        System.out.println(point);
        DistanceMatcher matcher2 = new DistanceMatcher();
        start = System.currentTimeMillis();
        Optional<Endpoint> point2 = matcher2.match(20, 29.8831156, 121.574495);
        System.out.println(System.currentTimeMillis() - start);
        System.out.println(point2);

    }
}
