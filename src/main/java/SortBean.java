public class SortBean {
    public SortBean(Endpoint endpoint, double distance) {
        this.endpoint = endpoint;
        this.distance = distance;
    }

    Endpoint endpoint;
    double distance;
}
