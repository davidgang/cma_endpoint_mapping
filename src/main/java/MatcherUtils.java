import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class MatcherUtils {
    public Stream<Endpoint> parseFile(String file){
        //Create the CSVFormat object
        CSVFormat format = CSVFormat.RFC4180.withHeader().withDelimiter(',');

        //initialize the CSVParser object
        try {
            CSVParser parser = new CSVParser(new FileReader(file), format);
            return StreamSupport.stream(parser.spliterator(),false).map(record->this.getEndpoint(record));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    private Endpoint getEndpoint(CSVRecord record) {
        String pointCode = record.get("POINT CODE");
        String fullName = record.get("FULL NAME");
        String[] splitted = fullName.split(" - ");
        String city = splitted[0];
        double longitude = Double.parseDouble(record.get("LONGITUDE"));
        double latitude = Double.parseDouble(record.get("LATITUDE"));
        return new Endpoint(pointCode,city,fullName,latitude,longitude);
    }

    //https://stackoverflow.com/questions/3694380/calculating-distance-between-two-points-using-latitude-longitude-what-am-i-doi
    public double distance(double lat1, double lat2, double lon1,
                                  double lon2, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }
    public double distance(Endpoint e, double lat, double lon){
        return distance(e.getLatitude(), lat, e.getLongitude(), lon, 0,0);
    }
    public Optional<Endpoint> getNearestPoint(List<Endpoint> endpoints, double lat, double lon){
        Optional<SortBean> minBean = endpoints.stream().map(e -> new SortBean(e, distance(e, lat, lon))).min(Comparator.comparing(e -> e.distance));
        if(minBean.isPresent()) {
            return Optional.of(minBean.get().endpoint);
        }
        return Optional.empty();

    }
}
