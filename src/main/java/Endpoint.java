public class Endpoint {
    String pointCode;
    String city;
    String address;
    double latitude;
    double longitude;

    public String getPointCode() {
        return pointCode;
    }

    @Override
    public String toString() {
        return "Endpoint{" +
                "pointCode='" + pointCode + '\'' +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public Endpoint(String pointCode, String city, String address, double latitude, double longitude) {

        this.pointCode = pointCode;
        this.city = city;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
